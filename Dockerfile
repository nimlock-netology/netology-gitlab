FROM centos:7

ENV PYTHON_VER=3.8.7 \
    PYTHON_MINOR=3.8 \
    PORT=5290

RUN yum makecache \
    && yum install -y gcc openssl-devel bzip2-devel libffi-devel wget make

WORKDIR /opt

RUN wget https://www.python.org/ftp/python/${PYTHON_VER}/Python-${PYTHON_VER}.tgz \
    && tar xzf Python-${PYTHON_VER}.tgz \
    && cd Python-${PYTHON_VER} \
    && ./configure --enable-optimizations \
    && make altinstall \
    && rm -rf ../Python-${PYTHON_VER}.tgz ../Python-${PYTHON_VER}

WORKDIR /python_api

COPY requirements.txt src/python-api.py ./

RUN pip${PYTHON_MINOR} install -r requirements.txt

EXPOSE ${PORT}

CMD python${PYTHON_MINOR} python-api.py
